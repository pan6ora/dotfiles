```
  █▀▀▄   ▄▄   ▄  ▄  ▄▀▀    ▄▄   ▄▄▄    ▄▄ 
  █▀▀   █▄▄█  █▀▄█  █▀▀▄  █  █  █▄▄▀  █▄▄█
  ▀     ▀  ▀  ▀  ▀   ▀▀    ▀▀   ▀  ▀  ▀  ▀
    Rémy Le Calloch  | remy.lecalloch.net
 ┌────────────────────────────────────────┐
 |   My PCs configuration and dotefiles   |
 └────────────────────────────────────────┘
                                          
Declarative configuration using Nix on NixOS
```

# Software

## Environment

| Program               | Name                                                  |
| :-------------------- | :-----------------------------------------------------|
| Linux Distribution    | [NixOS](https://nixos.org/)                           |
| Audio server          | [PipeWire](https://pipewire.org)                      |
| Window System         | [X](https://www.x.org/wiki/)                          |
| Window Manager        | [i3](https://i3wm.org/)                               |
| Bar                   | [polybar](https://github.com/jaagr/polybar)           |
| Terminal Emulator     | [Kitty](https://sw.kovidgoyal.net/kitty/)             |
| Program Launcher      | [rofi](https://github.com/DaveDavenport/rofi)         |
| Shell                 | [zsh](https://www.zsh.org/)                           |
| Notifications manager | [Dunst](https://dunst-project.org/)                   |
| Password manager      | [Pass](https://passwordstore.org)                     |

## Essentials

| Program               | Name                                                  |
| :-------------------- | :-----------------------------------------------------|
| Web Browser           | [Firefox](https://mozilla.org/)                       |
| Code Editor           | [Neovim](https://neovim.io/)                          |
| Music player          | [cmus](https://cmus.github.io/)                       |
| Email client          | [aerc](https://aerc-mail.org/)                        |
| Matrix client         | element-desktop                                       |

## Other

| Program               | Name                                                  |
| :-------------------- | :-----------------------------------------------------|
| Graphic Editors       | Gimp                                                  |
|                       | Inkscape                                              |
| Photography Editor    | Darktable                                             |
| Photo Manager         | Digikam                                               |
| Office Suite          | LibreOffice                                           |
| Video Editing         | Shotcut                                               |
| 3D Modeling           | OpenSCAD                                              |
| MAO                   | Ardour                                                |