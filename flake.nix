{
  description = "Pan6ora-Nix configuration";

  inputs = {
    nixpkgs-unstable.url      = github:nixos/nixpkgs/nixos-unstable;
    nixpkgs.url               = github:nixos/nixpkgs/nixos-23.11;
    nur.url                   = github:nix-community/NUR;
    nixvim.url                = github:pta2002/nixvim;
    nix-colors.url            = github:misterio77/nix-colors;
    flake-utils.url           = github:numtide/flake-utils;
    nixos-hardware.url        = github:NixOS/nixos-hardware/master;	
    musnix.url                = github:musnix/musnix;
    home-manager = {
      url                     = github:nix-community/home-manager/release-23.11;
      inputs.nixpkgs.follows  = "nixpkgs";
    };
  };

  outputs = {
    nur,
    self,
    nixvim,
    musnix,
    nixpkgs,
    nix-colors,
    flake-utils,
    home-manager,
    nixos-hardware,
    nixpkgs-unstable,
  }:

  let
    system              = "x86_64-linux";
    nurModule           = nur.nixosModules.nur;
    musnixModule        = musnix.nixosModules.musnix;
    #nixvimModule        = nixvim.homeManagerModules.nixvim;
    home-managerModule  = home-manager.nixosModules.home-manager;
    hardwareModule      = nixos-hardware.nixosModules.lenovo-thinkpad-x230;
    nixpkgs-outPath.environment.etc."nix/inputs/nixpkgs".source = nixpkgs.outPath;
    unstable = import nixpkgs-unstable {inherit system;};
  in rec {
    nixosConfigurations = {
      x230 = nixpkgs.lib.nixosSystem {
        inherit system;
        specialArgs = {
          inherit unstable;
        };
        modules = [
          ./os/hubris
          ./os/x230
          nurModule
          musnixModule
          hardwareModule
          nixpkgs-outPath
          home-managerModule {
            home-manager = {
              useGlobalPkgs = true;
              useUserPackages = true;
              users.pan6ora = {
                imports = [
                ./home/x230.nix
                #nixvimModule
                nurModule
                ];
              };
              extraSpecialArgs = {
                #inherit nixvim;
                main_monitor = "LVDS-1";
                config_name = "x230";
                screen_fingerprint = "00ffffffffffff0030e4d3020000000000150103801c1078ea10a59658578f2820505400000001010101010101010101010101010101381d56d45000163030202500159c1000001b000000000000000000000000000000000000000000fe004c4720446973706c61790a2020000000fe004c503132355748322d544c423100f7";
                inherit nix-colors;
                inherit unstable;
              };
            };
          }
        ];
      };
      x280 = nixpkgs.lib.nixosSystem {
        inherit system;
        specialArgs = {
          inherit unstable;
        };
        modules = [
          ./os/hubris
          ./os/x280
          nurModule
          musnixModule
          hardwareModule
          nixpkgs-outPath
          home-managerModule {
            home-manager = {
              useGlobalPkgs = true;
              useUserPackages = true;
              users.pan6ora = {
                imports = [
                ./home/x280.nix
                #nixvimModule
                nurModule
                ];
              };
              extraSpecialArgs = {
                #inherit nixvim;
                main_monitor = "eDP-1";
                config_name = "x280";
                screen_fingerprint = "00ffffffffffff0006af6c100000000026160104951c107802867597524b89242a505400000001010101010101010101010101010101881d56e25000163026163600149b100000180000000f0000000000000000000000000020000000fe0041554f0a202020202020202020000000fe004231323558544e30312e30200a00ec";
                inherit nix-colors;
                inherit unstable;
              };
            };
          }
        ];
      };
    };
  } // flake-utils.lib.eachDefaultSystem 
  (system:
    let
      overlay = (final: prev: { });
      overlays = [ overlay ];
      pkgs = import nixpkgs {
        inherit system overlays;
        config.allowUnfree = true; 
      };
    in rec {
      inherit overlay overlays;
      devShells = {
        micromamba = (pkgs.buildFHSUserEnv {
          name = "micromamba";
          targetPkgs = pkgs: with pkgs; [ micromamba ];
          runScript = "bash --init-file $HOME/Config/scripts/pan6-micromamba";
        }).env;
      };
    }
  );
}
