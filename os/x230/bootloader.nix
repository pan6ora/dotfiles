{
    boot.loader = {
        timeout = 2;
        grub = {
            enable = true;
            devices = [
              "/dev/sda"  
            ];
            useOSProber = true;
        };
    };
}