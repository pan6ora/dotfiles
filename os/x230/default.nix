{ pkgs, ... }:

{
  imports = [
    ./hardware.nix
    ./bootloader.nix
  ];   
}