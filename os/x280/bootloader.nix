{
    # if boot on windows instead of bios:
    # remove windows from boot options in bios
    
    boot.loader = {
        timeout = 6;
        systemd-boot = {
            enable = false;
        };
        efi = {
            efiSysMountPoint = "/boot";
        };
        efi.canTouchEfiVariables = false;
        grub = {
            enable = true;
            device = "nodev";
            efiSupport = true;
            useOSProber = true;
            efiInstallAsRemovable = true;
        };
    };
}