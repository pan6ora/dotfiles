{ config, lib, pkgs, ... }:
{  
  # allow to communicate with smartcards
  services.pcscd.enable = true;

  # use gpg-agent instead of ssh-agent
  programs.ssh.startAgent = true;
  #environment.shellInit = "
  #  gpg-connect-agent /bye
  #  export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
  #";

  environment.systemPackages = with pkgs; [
    gnupg
  ];

  # SETUP YUBIKEYS
  #   ```
  #   # gpg --keyserver keyserver.ubuntu.com --recv-keys ee7254e7c7649335
  #   # gpg --card-status
  #   # gpg --edit-key ee7254e7c7649335
  #   # gpg> trust
}