# Thinkpad T440 

# postconditions:
# 1) status should be enabled:
# cat /proc/acpi/ibm/fan
# 2) No errors in systemd logs:
# journalctl -u thinkfan.service -f
{
  services.thinkfan = {
    enable = false;
    #preStart = "/run/current-system/sw/bin/modprobe  -r thinkpad_acpi && /run/current-system/sw/bin/modprobe thinkpad_acpi";
    levels = [
    [0     0      42]
    [1     40     47]
    [2     45     52]
    [3     50     57]
    [4     55     62]
    [5     60     77]
    [7     73     93]
    [127   85     32767]
    ];
  };
}
