{
  environment.pathsToLink = [ "/libexec" ];
  
  services = {
    xserver = {
      enable = true;
      serverFlagsSection = ''
        Option "BlankTime" "0"
        Option "StandbyTime" "0"
        Option "SuspendTime" "0"
        Option "OffTime" "0"
      '';

      # Configure keymap in X11
      layout = "fr";
      xkbVariant = "oss_latin9";

      # Symlink configuration under /etc/X11/xorg.conf
      exportConfiguration = true;

      desktopManager = {
        xterm.enable = false;
      };

      displayManager = {
        defaultSession = "none+i3";
      };

      windowManager.i3 = {
        enable = true;
      };
      libinput = {
        enable = true;
        touchpad = {
          naturalScrolling = false;
          middleEmulation = true;
          tapping = true;
        };
      };
    };
    colord.enable = true;
  };
}
