{ pkgs, ... }:

{
  system.stateVersion = "23.11";
  
  imports = [
    ./nix.nix
    ./security.nix
    ./users.nix
    ./xorg.nix
    ./gpg.nix
    ./zsh.nix
    ./pipewire.nix
    ./thinkfan.nix
  ];    
  
  environment.systemPackages = with pkgs; [
    git
    gparted
    veracrypt
  ];

  # Time zone
  time.timeZone = "Europe/Paris";

  # Internationalisation properties
  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings = {
      LC_ADDRESS        = "fr_FR.UTF-8";
      LC_IDENTIFICATION = "fr_FR.UTF-8";
      LC_MEASUREMENT    = "fr_FR.UTF-8";
      LC_MONETARY       = "fr_FR.UTF-8";
      LC_NAME           = "fr_FR.UTF-8";
      LC_NUMERIC        = "fr_FR.UTF-8";
      LC_PAPER          = "fr_FR.UTF-8";
      LC_TELEPHONE      = "fr_FR.UTF-8";
      LC_TIME           = "fr_FR.UTF-8";
    };
  };
  
  console = {
    font = "Lat2-Terminus16";
    keyMap = "fr";
  };

  networking = {
    networkmanager.enable = true;
    firewall.enable = true;
    hostName = "hubris";
  };

  # real-time audio
  musnix = {
    enable = true;
    #kernel.realtime = true;  # warning ! kernel compilation (> 20h)
  };
  users.extraGroups.audio.members = [ "pan6ora" ];

  # virtal machines
  virtualisation.virtualbox.host.enable = true;
  users.extraGroups.vboxusers.members = [ "pan6ora" ];

  # docker
  virtualisation.docker.enable = true;
  users.extraGroups.docker.members = [ "pan6ora" ];

  services.udisks2.enable = true;
  services.blueman.enable = true;
  services.gvfs.enable = true;
}
