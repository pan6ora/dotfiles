{
    users = {
        mutableUsers = false;
        users = {
            root = {
                isSystemUser = true;
                hashedPassword = "$6$9JiH4WlZCKO5Gbbh$y9J53agttEbYPajvteDEDnP6GhXshWnSYt1Yn9Mte7ufAvAqtPnMaw4jCVIdxjHD8voRFugnQuAvTWjZNDS2T0";
            };
            pan6ora = {
                isNormalUser = true;
                uid = 1000;
                group = "pan6ora";
                extraGroups = [
                    "wheel"             # use sudo
                    "networkmanager"    # add/edit network connections
                ];
                hashedPassword = "$6$Ws5SjtrJVOh27gMh$vgPro16Ie3a3Ogqh3kXNxH2cWCX/Itv2ZoQ9QlOnhXaYQcvWZpFHpU2zUHvvfL2Klt/SpZvvuOmN9ZFgDKkB31";
            };
        };
        groups = {
            pan6ora.gid = 1000;
        };
    };
}
