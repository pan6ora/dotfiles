{pkgs, config, unstable, ... }:

{
  home.packages = with pkgs; [
    # MULTIMEDIA
    cmus                # music player
    vlc                 # video player
    feh                 # image displayer
    darktable           # raw photo editor
    digikam             # gallery manager
    gimp                # image editor
    inkscape            # vectorial drawing
    libreoffice-fresh   # office suite
    openscad            # textual 3D modelling
    libsForQt5.kdenlive # clip editor
    ardour              # MAO software
    carla               # Soundfonts reader
    easyeffects         # sound effect manager
    # calf               # set of audio plugins
    linuxsampler        # audio sampler
    # COMMUNICATION
    discord             # Discord desktop client
    zoom-us             # Zoom desktop client
    signal-desktop      # Signal desktop client
    element-desktop     # Matrix desktop client
    # PROGRAMMING LANGUAGES
    python3                 # python 
    python311Packages.pip   # python package manager
    sphinx                  # python documentation
    gnumake                 # make command
    # SOUND MANAGEMENT
    qpwgraph              # pipewire graph for sound I/O management
    qsampler              # create and manager virtual instruments
    wireplumber           # necessary for a compatibility reason (I guess)
    openssh               # ssh
    # DIVERS
    dunst               # notifications display
    ncpamixer           # TUI pavucontrol equivalent
    bluetuith           # TUI bluetooth manager
    displaycal          # screen color calibration
    zsh-nix-shell       # use zsh in nix-shell
    #volctl              # per-application volume control      
    # khard              # console address book
    xfce.thunar         # file manager
    arandr                # GUI monitors management
    neovim                # vim advanced replacement
    filelight             # explore disk usage
    sshfs                 # mount filesystem using ssh
    rsync                 # sync local and/or remote directories
    pandoc                # convert text files from/to divers formats
    texlive.combined.scheme-basic    # latex library
    pinentry-qt           # password dialog box
    pwgen                 # generate passwords
    xiccd                 # manage x color profiles (for DisplayCal)
    p7zip                 # archives manager (multi-formats)
    docker                # create and launch containers
    docker-compose        # launch multiple docker containers
    pam_u2f               # use U2F auth with Yubikeys
    paperkey              # convert GPG keys to printable format
    vim                   # simple text editor in case neovim is down 
    cups                  # open source printer
    which                 # display location of an executable
    jq                    # command-line json processor (for i3)
    libnotify             # send desktop notifications
    spectacle             # screenshot utility
    keepassxc             # password manager
  ];

  xdg.configFile.khard.source       = ./includes/khard;
  xdg.configFile.vdirsyncer.source  = ./includes/vdirsyncer;
  #xdg.configFile.keepassxc.source   = ./includes/keepassxc;
  #xdg.configFile."DisplayCAL/displaycal.ini".source  = ./includes/displaycal/config/displaycal.ini;

  #home.file.".local/share/DisplayCAL/storage".source = ./includes/displaycal/storage;
  home.file.".condarc".source = ./includes/condarc;
  home.file.".pandoc".source = ./includes/pandoc;
}