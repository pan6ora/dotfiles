{ config, pkgs, ... }:

{ 
  imports = [
    ./pan6ora-gui.nix
  ];

  home = {
    homeDirectory = "/home/pan6ora";
    username = "pan6ora";
    file = {
      "Libraries".source = config.lib.file.mkOutOfStoreSymlink "/mnt/Storage/Libraries";
      "Projects".source = config.lib.file.mkOutOfStoreSymlink "/mnt/Storage/Projects";
    };
  };
}