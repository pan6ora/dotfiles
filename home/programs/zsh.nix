{ config, pkgs, ... }:


{
  programs = {
    zsh = {
      enable = true;

      dotDir = ".config/zsh";

      enableAutosuggestions = true;
      syntaxHighlighting.enable = true;

      completionInit = "autoload -U compinit && compinit && prompt walters";
      history = {
          path = "${config.xdg.stateHome}/zsh_history";
          size = 50000;
      };
    };
  };
}
