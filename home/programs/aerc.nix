{ pkgs, config, ... }:
{
  programs = {
    aerc = {
      enable = true;
      extraConfig = {
        general.unsafe-accounts-conf = true;
        ui = {
          index-format = "| %-60.58s %-40.38F %15.15D";
          timestamp-format = "02/01/06";
          this-day-time-format = "15:04";
          this-week-time-format= "Monday 15:04";
          this-year-time-format= "02 January";
          sidebar-width= "30";
          mouse-enabled= true;
          new-message-bell= false;
          dirlist-tree= true;
          border-char-vertical= "\" \"";
          border-char-horizontal= "\"―\"";
          styleset-name= "default";
        };
        viewer = {
          header-layout = "From,To,Cc|Bcc,Date,Subject";
        };
        compose = {
          editor = "vim";
          header-layout = "To,From,CC,Subject";
          address-book-cmd = "khard email --parsable --search-in-source-files --remove-first-line %s";
          reply-to-self = false;
        };
        filters = {
          "text/plain"              = "colorize";
          "text/calendar"           = "calendar";
          #"message/delivery-status" = "colorize";
          #"message/rfc822"          = "colorize";
          #"text/html"       = "pandoc -f html -t plain | colorize";
        };
        triggers = {
          new-email= "exec notify-send \"New email from %n\" \"%s\"";
        };
      };
      extraBinds = {
        global = {
          "<C-Left>" = ":prev-tab<Enter>";
          "<C-Right>" = ":next-tab<Enter>";
        };
        messages = {
          m           = ":move ";
          rr          = ":reply -aq<Enter>";
          "<C-Down>"  = ":next-folder<Enter>";
          "<C-Up>"    = ":prev-folder<Enter>";
          "<Down>"    = ":next<Enter>";
          "<Up>"      = ":prev<Enter>";
          "<PgDn>"    = ":next 100%<Enter>";
          "<PgUp>"    = ":prev 100%<Enter>";
          "<Enter>"   = ":view<Enter>";
          "<Esc>"     = ":clear<Enter>";
          q           = ":quit<Enter>";
          g           = ":select 0<Enter>";
          G           = ":select -1<Enter>";
          v           = ":mark -t<Enter>";
          V           = ":mark -v<Enter>";
          T           = ":toggle-threads<Enter>";
          D           = ":delete<Enter>";
          A           = ":archive flat<Enter>";
          c           = ":compose<Enter>";
          u           = ":check-mail<Enter>";
          "/"         = ":search<space>";
          "\\"        = ":filter<space>";
          n           = ":next-result<Enter>";
          N           = ":prev-result<Enter>";
        };
        "messages:folder=Drafts" = {
          "<Enter>" = ":recall<Enter>";
        };
        view = {
          rr        = ":reply -aq<Enter>";
          "/"       = ":toggle-key-passthrough<Enter>/";
          q         = ":close<Enter>";
          O         = ":open<Enter>";
          S         = ":save<space>";
          "|"       = ":pipe<space>";
          D         = ":delete<Enter>";
          A         = ":archive flat<Enter>";
          o         = ":open-link <space>";
          f         = ":forward<Enter>";
          rq        = ":reply -aq<Enter>";
          Rr        = ":reply<Enter>";
          Rq        = ":reply -q<Enter>";
          H         = ":toggle-headers<Enter>";
          "<C-Down>"= ":next-part<Enter>";
          "<C-Up>"  = ":prev-part<Enter>";
        };
        "view::passthrough" = {
          "$noinherit"  = true;
          "$ex"         = "<C-x>";
          "<Esc>"       = ":toggle-key-passthrough<Enter>";
        };
        compose = {
          "<C-Left>"    = ":prev-tab<Enter>";
          "<C-Right>"   = ":next-tab<Enter>";
          "<C-a>"       = ":attach";
          "<tab>"       = ":next-field<Enter>";
          "<backtab>"   = ":prev-field<Enter>";
          "$noinherit"  = true;
          "$ex"         = "<C-x>";
        };
        "compose::editor" = {
          "<C-Down>"    = ":next-field<Enter>";
          "<C-Up>"      = ":prev-field<Enter>";
          "<C-Left>"    = ":prev-tab<Enter>";
          "<C-Right>"   = ":next-tab<Enter>";
          "$noinherit"  = true;
          "$ex"         = "<C-x>";
        };
        "compose::review" = {
          y = ":send<Enter>";
          q = ":abort<Enter>";
          v = ":preview<Enter>";
          p = ":postpone<Enter>";
          e = ":edit<Enter>";
          a = ":attach<space>";
          d = ":detach<space>";
        };
        terminal = {
          "<C-Left>"    = ":prev-tab<Enter>";
          "<C-Right>"   = ":next-tab<Enter>";
          "$noinherit"  = true;
          "$ex"         = "<C-x>";
        };
      };
      stylesets = {
        default = {
          "*.default"                   = true;
          "*.selected.reverse"          = "toggle";
          "title.reverse"               = true;
          "header.bold"                 = true;
          "*error.bold"                 = true;
          "error.fg"                    = "red";
          "warning.fg"                  = "yellow";
          "success.fg"                  = "green";
          "statusline*.default"         = true;
          "statusline_default.reverse"  = true;
          "statusline_error.fg"         = "red";
          "statusline_error.reverse"    = true;
          #"statusline_warning.fg"       = "yellow";
          #"statusline_warning.reverse"  = true;
          "msglist_unread.bold"         = true;
          "msglist_deleted.fg"          = "gray";
          #"msglist_result.fg"           = "green";
          "completion_pill.reverse"     = true;
          "tab.reverse"                 = false;
          "border.reverse"              = false;
          "selector_focused.reverse"    = true;
          "selector_chooser.bold"       = true;
          "msglist_marked.bg"           = "cornflowerblue";
        };
      };
    };
  };
}
