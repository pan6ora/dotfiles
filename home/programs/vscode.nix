{ config, pkgs,... }:
{
    programs.vscode = {
        enable = true;
        package = pkgs.vscodium;
	    mutableExtensionsDir = false;

        extensions = with pkgs.vscode-extensions; [
            ms-azuretools.vscode-docker
            jdinhlife.gruvbox
            mhutchie.git-graph
            streetsidesoftware.code-spell-checker
            vscodevim.vim
            esbenp.prettier-vscode
            ms-python.python
            bbenoist.nix
            donjayamanne.githistory
            yzhang.markdown-all-in-one
	    tomoki1207.pdf
        ]
        ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
            {
                name = "code-spell-checker-french";
                publisher = "streetsidesoftware";
                version = "0.4.1";
                sha256 = "c93a72bece2369be47dd8e5a4780964d9596f962156c132564c67bfec26fac13";
            }
        ];
        keybindings =
        [
            {
                key = "alt+space";
                command = "workbench.action.togglePanel";
                when = "";
            }
            {
                key = "alt+x";
                command = "workbench.action.toggleAuxiliaryBar";
                when = "";
            }
            {
                key = "alt+down";
                command = "workbench.action.focusPanel";
                when = "";
            }
            {
                key = "alt+up";
                command = "workbench.action.focusActiveEditorGroup";
                when = "";
            }
            {
                key = "alt+left";
                command = "workbench.action.focusAuxiliaryBar";
                when = "";
            }
            {
                key = "ctrl+left";
                command = "workbench.action.previousEditor";
                when = "";
            }
            {
                key = "ctrl+right";
                command = "workbench.action.nextEditor";
                when = "";
            }
        ];
        userSettings = {
            "extensions.autoUpdate" = false;
            "workbench.statusBar.visible" = false;
            "window.menuBarVisibility" = "toggle";
            "workbench.sideBar.location" = "right";
            "workbench.startupEditor" = "none";
            "workbench.activityBar.location" = "hidden";
            "cSpell.language" = "en,fr";
            "window.density.editorTabHeight" = "compact";
            "workbench.colorTheme" = "Gruvbox Dark Hard";
            "workbench.tips.enabled" = false;
            "workbench.tree.indent" = 12;
            "workbench.tree.renderIndentGuides" = "always";
            "security.workspace.trust.untrustedFiles"= "open";
            "git.confirmSync"= false;
        };
    };
}
