{ config, pkgs, ... }:
let
  inherit (config.lib.formats.rasi) mkLiteral;
in {
  programs.rofi = {
    enable = true;
    theme = {
      "*" = {
        foreground                  = mkLiteral "#${config.colorScheme.palette.base07}";
        background                  = mkLiteral "#${config.colorScheme.palette.base00}";
        selected-normal-foreground  = mkLiteral "#${config.colorScheme.palette.base0F}";
        selected-normal-background  = mkLiteral "#${config.colorScheme.palette.base08}";
        red                         = mkLiteral "#${config.colorScheme.palette.base09}";
        blue                        = mkLiteral "#${config.colorScheme.palette.base0C}";
        active-foreground           = mkLiteral "#${config.colorScheme.palette.base0F}";

        separatorcolor                = mkLiteral "@background";
        background-color              = mkLiteral "@background";

        normal-foreground             = mkLiteral "@foreground";
        alternate-normal-foreground   = mkLiteral "@foreground";
        selected-active-foreground    = mkLiteral "@active-foreground";
        alternate-active-foreground   = mkLiteral "@active-foreground";
        selected-urgent-foreground    = mkLiteral "@selected-normal-foreground";
        urgent-foreground             = mkLiteral "@red";
        alternate-urgent-foreground   = mkLiteral "@red";

        normal-background             = mkLiteral "@background";
        urgent-background             = mkLiteral "@background";
        active-background             = mkLiteral "@background";
        alternate-normal-background   = mkLiteral "@background";
        alternate-urgent-background   = mkLiteral "@background";
        alternate-active-background   = mkLiteral "@background";

        border-color                  = mkLiteral "@foreground";
        bordercolor                   = mkLiteral "@background";

        selected-urgent-background    = mkLiteral "@selected-normal-background";
        selected-active-background    = mkLiteral "@selected-normal-background";

        spacing = 2;
      };
      "window" = {
        background-color  = mkLiteral "@background";
        border            = 1;
        padding           = 5;
      };
      "mainbox" = {
        border            = 0;
        padding           = 0;
      };
      "message" = {
        border            = mkLiteral "1px dash 0px 0px";
        border-color      = mkLiteral "@separatorcolor";
        padding           = mkLiteral "1px";
      };
      "textbox" = {
        text-color        = mkLiteral "@foreground";
      };
      "listview" = {
        fixed-height      = 0;
        border            = mkLiteral "2px dash 0px 0px";
        border-color      = mkLiteral "@separatorcolor";
        spacing           = mkLiteral "2px";
        scrollbar         = mkLiteral "true";
        padding           = mkLiteral "2px 0px 0px";
      };
      "element" = {
        border            = 0;
        padding           = mkLiteral "1px";
      };
      "element-text" = {
        background-color  = mkLiteral "inherit";
        text-color        = mkLiteral "inherit";
      };
      "element.normal.normal" = {
        background-color  = mkLiteral "@normal-background";
        text-color        = mkLiteral "@normal-foreground";
      };
      "element.normal.urgent" = {
        background-color  = mkLiteral "@urgent-background";
        text-color        = mkLiteral "@urgent-foreground";
      };
      "element.normal.active" = {
        background-color  = mkLiteral "@active-background";
        text-color        = mkLiteral "@active-foreground";
      };
      "element.selected.normal" = {
        background-color  = mkLiteral "@selected-normal-background";
        text-color        = mkLiteral "@selected-normal-foreground";
      };
      "element.selected.urgent" = {
        background-color  = mkLiteral "@selected-urgent-background";
        text-color        = mkLiteral "@selected-urgent-foreground";
      };
      "element.selected.active" = {
        background-color  = mkLiteral "@selected-active-background";
        text-color        = mkLiteral "@selected-active-foreground";
      };
      "element.alternate.normal" = {
        background-color  = mkLiteral "@background";
        text-color        = mkLiteral "@alternate-normal-foreground";
      };
      "element.alternate.urgent" = {
        background-color  = mkLiteral "@alternate-urgent-background";
        text-color        = mkLiteral "@alternate-urgent-foreground";
      };
      "element.alternate.active" = {
        background-color  = mkLiteral "@alternate-active-background";
        text-color        = mkLiteral "@alternate-active-foreground";
      };
      "scrollbar" = {
        width             = mkLiteral "4px";
        border            = 0;
        handle-width      = mkLiteral "8px";
        padding           = 0;
      };
      "mode-switcher" = {
        border            = mkLiteral "2px dash 0px 0px";
        border-color      = mkLiteral "@separatorcolor";
      };
      "button.selected" = {
        background-color  = mkLiteral "@selected-normal-background";
        text-color        = mkLiteral "@selected-normal-foreground";
      };
      "inputbar" = {
        spacing           = 0;
        text-color        = mkLiteral "@normal-foreground";
        padding           = mkLiteral "1px";
      };
      "case-indicator" = {
        spacing           = 0;
        text-color        = mkLiteral "@normal-foreground";
      };
      "entry" = {
        spacing           = 0;
        text-color        = mkLiteral "@normal-foreground";
      };
      "prompt" = {
        spacing           = 0;
        text-color        = mkLiteral "@normal-foreground";
      };
      "inputbar" = {
        children          = mkLiteral "[prompt,textbox-prompt-colon,entry,case-indicator]";
      };
      "textbox-prompt-colon" = {
        expand            = mkLiteral "false";
        str               = "";
        margin            = mkLiteral "0px 0.3em 0em 0em";
        text-color        = mkLiteral "@normal-foreground";
      };
    };
  };
}