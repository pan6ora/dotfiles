{ config, pkgs, main_monitor,... }:
{
  services.polybar = {
    enable = true;
    package = pkgs.polybar.override {
      i3Support = true;
    };
    config = {
      "colors" = {
        background = "#${config.colorScheme.palette.base00}";
        background-alt = "#${config.colorScheme.palette.base08}";
        foreground = "#${config.colorScheme.palette.base0F}";
        foreground-alt = "#${config.colorScheme.palette.base07}";
        color0 = "#${config.colorScheme.palette.base08}";
        color1 = "#${config.colorScheme.palette.base09}";
        color2 = "#${config.colorScheme.palette.base0A}";
        color3 = "#${config.colorScheme.palette.base0B}";
        color4 = "#${config.colorScheme.palette.base0C}";
        color5 = "#${config.colorScheme.palette.base0D}";
        color6 = "#${config.colorScheme.palette.base0E}";
        color7 = "#${config.colorScheme.palette.base0F}";
      };
      "bar/bar1" = {
        monitor = "${main_monitor}";
        width = "56%";
        height = "4.5%";
        offset-x = "22%";
        offset-y = "0";
        radius = "0";
        fixed-center = "true";
        format-prefix = "bat: ";
        format-prefix-foreground = "\${colors.foreground-alt}";
        enable-ipc = "true";
        background = "\${colors.background}";
        foreground = "\${colors.foreground}";
        line-size = "3";
        line-color = "\${colors.color3}";
        border-size = "1";
        border-top-size = "0";
        border-color = "\${colors.foreground}";
        padding-left = "6";
        padding-right = "6";
        module-margin-left = "2";
        module-margin-right = "2";
        font-0 = "fixed:pixelsize=10;1";
        font-1 = "unifont:fontformat=truetype:size=8:antialias=false;0";
        font-2 = "siji:pixelsize=10;1";
        modules-left = "date";
        modules-center = "i3";
        modules-right = "memory cpu battery";
        override-redirect = "true";
        tray-position = "right";
        tray-padding = "3";
        tray-background = "\${colors.background}";
        tray-foreground = "\${colors.foreground}";
      };
      "bar/bar2" = {
        "inherit" = "bar/bar1";
        monitor = "\${env:MONITOR_2:none}";
        width = "40%";
        height = "20";
        offset-x = "33%";
      };
    };
    settings = {
      "module/i3" = {
        type = "internal/i3";
        format = "<label-state> <label-mode>";
        index-sort = "true";
        wrapping-scroll = "false";
        pin-workspaces = "true";
        label-mode-padding = "2";
        label-mode-foreground = "#000";
        label-mode-background = "\${colors.color3}";
        label-focused = "%index%";
        label-focused-background = "\${colors.background-alt}";
        label-focused-underline= "\${colors.color3}";
        label-focused-padding = "2";
        label-unfocused = "%index%";
        label-unfocused-padding = "2";
        label-visible = "%index%";
        label-visible-background = "\${self.label-focused-background}";
        label-visible-underline = "\${self.label-focused-underline}";
        label-visible-padding = "\${self.label-focused-padding}";
        label-urgent = "%index%";
        label-urgent-background = "\${colors.color1}";
        label-urgent-padding = "2";
      };
      "module/cpu" = {
        type = "internal/cpu";
        interval = "2";
        format-prefix = "cpu: ";
        format-prefix-foreground = "\${colors.foreground-alt}";
        label = "%percentage:2%%";
      };
      "module/memory" = {
        type = "internal/memory";
        interval = "2";
        format-prefix = "mem: ";
        format-prefix-foreground = "\${colors.foreground-alt}";
        label = "%percentage_used%%";
      };
      "module/date" = {
        type = "internal/date";
        interval = "5";
        date = "%d/%m/%Y   ";
        time = "   %H:%M";
        label = "%date% %time%";
      };
      "module/battery" = {
        type = "internal/battery";
        battery = "BAT0";
        adapter = "AC";
        full-at = "98";
        format-charging-underline = "\${colors.color6}";
        format-discharging-underline = "\${colors.color3}";
        format-full-underline = "\${colors.color6}";
      };
      "settings" = {
        screenchange-reload = "true";
        pseudo-transparency = "true";
      };
      "global/wm" = {
        margin-top = "5";
        margin-bottom = "5";
      };
    };
    script = "
MONITOR_2=$(/run/current-system/sw/bin/xrandr --listactivemonitors | /run/current-system/sw/bin/grep 1: | /run/current-system/sw/bin/awk 'NF{ print $NF }') polybar bar2 &
polybar bar1 &
    ";
  };
}