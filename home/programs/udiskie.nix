{ config, pkgs, ... }:
{
  # necessary on NixOS
  #services.udisks2.enable = true;

  services.udiskie = {
    enable = true;
    settings = {
      program_options = {
        menu = "flat";
        file_manager = "${builtins.elemAt config.xdg.mimeApps.defaultApplications."inode/directory" 0}";
        terminal = "i3-sensible-terminal";
      };
      notifications = {
        timeout = 1.5;
        device_mounted = 5;
      };
      quickmenu_actions = [
        "mount"
        "unmount"
        "unlock"
        "terminal"
        "detach"
        "delete"
      ];
      notification_actions = {
        device_mounted = [
          "browser"
        ];
        device_added = [
          "mount"
        ];
      };
    };
    tray = "auto";
    notify = true;
    automount = true;
  };
}