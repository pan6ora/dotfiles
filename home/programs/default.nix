{ pkgs, ... }:

{
  imports = [
    ./aerc.nix
    ./autorandr.nix
    ./firefox.nix
    ./git.nix
    ./gpg-agent.nix
    ./kitty.nix
    ./network-manager-applet.nix
    ./polybar.nix
    ./rofi.nix
    ./thunderbird.nix
    ./udiskie.nix
    ./vscode.nix
    ./zsh.nix
  ];
}