{ config, pkgs, ... }:
{ 
  # network connection trailer icon
  services = {
    network-manager-applet.enable = true;
  };
}