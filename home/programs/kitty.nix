{ config, pkgs, ... }:
{
  programs.kitty = {
    enable = true;
    settings = {
      background =      "#${config.colorScheme.palette.base00}";
      background-alt =  "#${config.colorScheme.palette.base08}";
      foreground =      "#${config.colorScheme.palette.base0F}";
      foreground-alt =  "#${config.colorScheme.palette.base07}";
      color0 =          "#${config.colorScheme.palette.base08}";
      color1 =          "#${config.colorScheme.palette.base09}";
      color2 =          "#${config.colorScheme.palette.base0A}";
      color3 =          "#${config.colorScheme.palette.base0B}";
      color4 =          "#${config.colorScheme.palette.base0C}";
      color5 =          "#${config.colorScheme.palette.base0D}";
      color6 =          "#${config.colorScheme.palette.base0E}";
      color7 =          "#${config.colorScheme.palette.base0F}";
    };
    font = {
      name = "'Droid Sans Mono', 'monospace'";
      size = 10;
    };
    settings = {
      window_padding_width = 8;
    };
  };
}