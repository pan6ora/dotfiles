{ config, pkgs, unstable, lib, ... }:
{

  programs.firefox = {
    enable = true;
    package = unstable.firefox.override {
      cfg = {
        extraNativeMessagingHosts = [
          pkgs.tridactyl-native
        ];
      };
    };

    # Check about:policies#documentation for options.
    policies = {
      DisableTelemetry = true;
      DisableFirefoxStudies = true;
      EnableTrackingProtection = {
        Value= true;
        Locked = true;
        Cryptomining = true;
        Fingerprinting = true;
      };
      DisablePocket = true;
      DisableFirefoxAccounts = true;
      DisableAccounts = true;
      DisableFirefoxScreenshots = true;
      OverrideFirstRunPage = "";
      OverridePostUpdatePage = "";
      DontCheckDefaultBrowser = true;
      DisplayBookmarksToolbar = "newtab";
      DisplayMenuBar = "default-off";
      SearchBar = "unified";
    };

    profiles = {
      pan6ora = {
        id = 0;
        isDefault = true;
        path = "pan6ora";
        settings = {
          "app.shield.optoutstudies.enabled" = false;
          "browser.aboutConfig.showWarning" = false;
          "browser.bookmarks.addedImportButton" = true;
          "browser.bookmarks.restore_default_bookmarks" = false;
          "browser.contentblocking.category" = "strict";
          "browser.download.useDownloadDir" = false;
          "browser.newtabpage.activity-stream.feeds.topsites" = false;
          "browser.newtabpage.activity-stream.showSearch" = false;
          "browser.newtabpage.enabled" = false;
          "browser.search.region" = "FR";
          "browser.startup.homepage" = "chrome://browser/content/blanktab.html";
          "browser.translations.automaticallyPopup" = false;
          "browser.translations.neverTranslateLanguages" = "fr";
          "browser.urlbar.placeholderName" = "Google";
          "browser.urlbar.quicksuggest.scenario" = "history";
          "browser.urlbar.shortcuts.bookmarks" = false;
          "browser.urlbar.shortcuts.history" = false;
          "browser.urlbar.shortcuts.tabs" = false;
          "browser.urlbar.showSearchSuggestionsFirst" = false;
          "browser.urlbar.suggest.openpage" = false;
          "browser.urlbar.suggest.topsites" = false;
          "datareporting.healthreport.uploadEnabled" = false;
          "extensions.formautofill.creditCards.enabled" = false;
          "intl.locale.requested" = "fr,en-US";
          "privacy.donottrackheader.enabled" = true;
          "privacy.fingerprintingProtection" = true;
          "privacy.globalprivacycontrol.enabled" = true;
          "privacy.trackingprotection.emailtracking.enabled" = true;
          "privacy.trackingprotection.enabled" = true;
          "privacy.trackingprotection.socialtracking.enabled" = true;
          "signon.rememberSignons" = false;
          # Needed to hide the tab bar
          "toolkit.legacyUserProfileCustomizations.stylesheets" = true;         
        };
        # Hide the tab bar
        userChrome = ''
          #TabsToolbar { visibility: collapse !important; }
        '';
        extensions = with config.nur.repos.rycee.firefox-addons; [
          french-dictionary
          i-dont-care-about-cookies
          sponsorblock
          ublock-origin
          keepassxc-browser
          canvasblocker
          # Not in NUR
          #   F.B. Purity
          #   rofi-tab-switcher
        ];
      };
    };
  };
  home.file.".mozilla/native-messaging-hosts/rofi_interface.json".source = ../includes/rofi-tab-switcher/rofi_interface.json;
}
