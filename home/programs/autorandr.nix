{ pkgs, config, screen_fingerprint, main_monitor, ... }:
let
  thinkvision_fingerprint = "00ffffffffffff0030aedd6100000000171e0104a51f12783aee95a3544c99260f5054bdcf84a94081008180818c9500950fa94ab300023a801871382d40582c450035ae1000001e000000fc004d31340a202020202020202020000000fd00324b1e5a14000a202020202020000000ff0056393036365748370affffffff0116020316b14b9005040302011f1213141165030c0010007c2e90a0601a1e4030203600dc0b1100001cab22a0a050841a3030203600dc0b1100001c662156aa51001e30468f3300dc0b1100001e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000008";
  asus_fingerprint = "00ffffffffffff000469c4270101010130190103803c2278ea53a5a756529c26115054afef808100814081809500b3008bc0a9400101023a801871382d40582c450056502100001e000000fd00324c1e530f000a202020202020000000fc0056433237390a20202020202020000000ff0046424c4d52533031383130350a010302031df14a900403021f1312110514230907078301000065030c001000023a801871382d40582c450056502100001e011d007251d01e206e28550056502100001e8c0ad08a20e02d10103e9600565021000018011d8018711c1620582c250056502100009e00000000000000000000000000000000000000000000000000008c";
  salon_parents_fingerprint = "00ffffffffffff000469e427163300001c1a0103683c22782a53a5a756529c26115054bfef00d1c0b30095008180814081c0714f0101023a801871382d40582c450056502100001e000000ff0047374c4d544a3031333037380a000000fd00324b185311000a202020202020000000fc00415355532056583237390a2020005e";
in {

  # to get infos use
  #   autorandr --fingerprint
  #   autorandr --config
  programs.autorandr = {
    enable = true;
    profiles = {
      "default" = {
        fingerprint = {
          ${main_monitor} = "${screen_fingerprint}";
        };
        config = {
          ${main_monitor} = {
            enable = true;
            crtc = 0;
            primary = true;
            position = "0x0";
            mode = "1366x768";
            rate = "60.00";
            gamma = "1.22:1.149:0.769";
          };
        };
      };
      "asus" = {
        fingerprint = {
          ${main_monitor} = "${screen_fingerprint}";
          HDMI-2 = "${asus_fingerprint}";
        };
        config = {
          ${main_monitor} = {
            enable = true;
            crtc = 0;
            primary = true;
            position = "277x1080";
            mode = "1366x768";
            rate = "60.00";
            gamma = "1.22:1.149:0.769";
          };
          HDMI-2 = {
            enable = true;
            crtc = 1;
            primary = false;
            position = "0x0";
            mode = "1920x1080";
            rate = "60.00";
          };
        };
      };           
      "thinkvision" = {
        fingerprint = {
          ${main_monitor} = "${screen_fingerprint}";
          DP-1 = "${thinkvision_fingerprint}";
        };
        config = {
          ${main_monitor} = {
            enable = true;
            crtc = 0;
            primary = true;
            position = "0x0";
            mode = "1366x768";
            rate = "60.00";
            gamma = "1.22:1.149:0.769";
          };
          DP-1 = {
            enable = true;
            crtc = 1;
            primary = false;
            position = "1366x0";
            mode = "1920x1080";
            rate = "60.00";
          };
        };
      };     
      "salon_parents" = {
        fingerprint = {
          ${main_monitor} = "${screen_fingerprint}";
          VGA-1 = "${salon_parents_fingerprint}";
        };
        config = {
          ${main_monitor}.enable = false;
          VGA-1 = {
            enable = true;
            crtc = 1;
            primary = true;
            position = "0x0";
            mode = "1920x1080";
            rate = "60.00";
          };
        };
      };
    };
  };
}