{pkgs, ... }:
{
    programs.git = {
        enable = true;
        userEmail = "remy@lecalloch.net";
        userName = "Rémy Le Calloch";

        extraConfig = {
            user.signingkey = "EEA7E16E4BA31A31";
            core.editor = "codium --wait";
            color.ui = "auto";
            push.default = "current";
            init.defaultBranch = "main";
            github.user = "Pan6ora";
            mergetool.prompt = false;
            commit.gpgsign = false;
            gpg.program = "gpg2";
            pull.rebase = false;
        };
    };
}
