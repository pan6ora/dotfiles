{ config, pkgs, unstable, lib, ... }:
{
    programs.thunderbird = {
        package = pkgs.thunderbird;
        enable = true;
        profiles.pan6ora = {
            isDefault = true;
            settings = {
            "toolkit.legacyUserProfileCustomizations.stylesheets" = true;
            "layout.css.devPixelsPerPx" = "0.92";
            };
            userChrome =
            ''
            #unifiedToolbar, #unifiedToolbarContainer {
                height: 0px !important;
                display: block !important;
            }
            #expendedsubjectBox {
                font-size:medium;
                line-height:1em;
                font-weight:medium;
            }
            '';
        };
    };
}