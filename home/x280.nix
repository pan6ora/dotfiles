{ config, pkgs, ... }:

{  
  imports = [
    ./pan6ora-gui.nix
  ];
  home = {
    homeDirectory = "/home/pan6ora";
    username = "pan6ora";
  };
}