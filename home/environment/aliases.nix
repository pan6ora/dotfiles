{pkgs, config, config_name, unstable, ... }:

{
  home.shellAliases = {
    # default options
    rm        = "rm -i";
    cp        = "cp -r";
    la        = "ls -a";
    ll        = "ls -l";
    lla       = "ls -al";
    mkdir     = "mkdir -p";
    shutdown  = "shutdown now";
    icat      = "kitty +kitten icat";

    # archives management
    "7zip"  = "7z a -t7z";
    un7zip  = "7z e -t7z";
    zip     = "7z a -tzip";
    unzip   = "7z e -tzip";
    gzip    = "7z a -tgzip";
    ungzip  = "7z e -tgzip";
    bzip    = "7z a -tbzip2";
    unbzip  = "7z e -tbzip2";
    lzip    = "7z a -tlzip";
    unlzip  = "7z e -tlzip";
    tar     = "7z a -ttar";
    untar   = "7z e -ttar";

    # applications
    open        = "xdg-open";
    sudo        = "doas";
    vim         = "nvim";
    audio       = "ncpamixer";
    pavucontrol = "ncpamixer";

    # nix
    switch        = "sudo nixos-rebuild switch --flake '${config.home.homeDirectory}/Config#${config_name}'";
    conda-update  = "nix develop --profile ${config.home.homeDirectory}/Config/envs/micromamba ${config.home.homeDirectory}/Config#micromamba";
    conda         = "nix develop ${config.home.homeDirectory}/Config/envs/micromamba";

    # scripts
    account   = "pan6-account";
    clean     = "pan6-utils clean";
    update    = "sudo pan6-utils update '${config.home.homeDirectory}/Config' ${config_name}";
    password  = "pwgen -s 20 -1 -y";

    # x230 specific
    wifi      = "rfkill unblock all";
  };
}