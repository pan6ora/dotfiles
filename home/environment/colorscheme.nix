{ config, nix-colors, ... }:

{
  imports = [
    nix-colors.homeManagerModule
  ];
  colorScheme = {
    slug = "pan6ora";
    name = "Pan6ora";
    author = "Rémy Le Calloch (https://remy.lecalloch.net)";
    palette = {
      base00 = "#1d2021";
      base08 = "#928374";
      base01 = "#cc241d";
      base09 = "#fb4934";
      base02 = "#98971a";
      base0A = "#b8bb26";
      base03 = "#d79921";
      base0B = "#fabd2f";
      base04 = "#458588";
      base0C = "#83a598";
      base05 = "#b16286";
      base0D = "#d3869b";
      base06 = "#689d6a";
      base0E = "#8ec07c";
      base07 = "#a89984";
      base0F = "#ebdbb2";
    };
  };
}
