{ config, pkgs, ... }:
{
  accounts.email = {
    maildirBasePath = "${config.home.homeDirectory}/Libraries/Mail";
    accounts = {
      main = {
        address = "remy@lecalloch.net";
        primary = true;
        aerc.enable = false;
        thunderbird = {
          enable = true;
          profiles =  [ "pan6ora" ];
        };
        imap = {
          host = "imap.migadu.com";
          port = 993;
        };
        smtp = {
          host = "smtp.migadu.com";
          port = 465;
        };
        userName = "remy@lecalloch.net";
        realName = "Rémy Le Calloch";
        aerc = {
          extraAccounts = {
            ault = "INBOX";
            p-starttls = false;
            y-to = "INBOX";
            ck-mail = "5m";
            default = "INBOX";
            copy-to = "INBOX";
          };
        };
      };
    };
  };
}