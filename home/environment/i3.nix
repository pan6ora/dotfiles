{ config, ... }:
{
  xsession.windowManager.i3 = {
    enable = true;
    extraConfig = "
      hide_edge_borders       vertical
      default_border          pixel 1
      default_floating_border pixel 1
      floating_minimum_size   75 x 50
      floating_maximum_size   1300 x 700
      bindcode 156 exec --no-startup-id i3lock -ec 33333C
    ";
    config = rec {
      modifier = "Mod4";
      bars = [ ];
      workspaceAutoBackAndForth = true;
      startup = [
        { command = "autorandr -c";
          always = true;
          notification = false; }
        { command = "displaycal-apply-profiles";
          always = true;
          notification = false; }
        { command = "systemctl --user restart polybar.service";
          always = true;
          notification = false; }
        { command = "xsetroot -solid \"#${config.colorScheme.palette.base00}\"";
          always = true;
          notification = false; }       
        { command = "timeout 1s xiccd";
          always = true;
          notification = false; }
        { command = "xset r rate 200 20";
          always = false;
          notification = false; }
        { command = "xinput set-prop 11 303 1";
          always = false;
          notification = false; }
        { command = "xinput set-prop 11 311 1";
          always = false;
          notification = false; }
        { command = "keepassxc";
          always = false;
          notification = false; }
      ];
      keybindings = {
        # show and hide polybar
        "${modifier}+w"                 = "exec polybar-msg cmd toggle";
        # general ("${modifier}+Shift+letter)
        "${modifier}+Shift+r"           = "restart";
        "${modifier}+Shift+a"           = "kill";
        # switch between modes
        "${modifier}+Shift+f"           = "fullscreen toggle";
        "${modifier}+Shift+space"       = "floating toggle";
        # execute programs in current workspace ("${modifier}+letter)
        "${modifier}+Return"            = "exec kitty";
        "${modifier}+space"             = "exec thunar";
        "${modifier}+d"                 = "exec rofi -show run";
        # execute programs in dedicated workspace (${modifi+letter)
        "${modifier}+a" = "workspace \"5\"; exec pan6-utils workspace_exec 5 firefox      firefox";
        "${modifier}+z" = "workspace \"6\"; exec pan6-utils workspace_exec 6 codium       codium";
        "${modifier}+e" = "workspace \"7\"; exec pan6-utils workspace_exec 7 cmus         kitty cmus";
        "${modifier}+r" = "workspace \"8\"; exec pan6-utils workspace_exec 8 thunderbird  thunderbird";
        "${modifier}+q" = "workspace \"9\"; exec pan6-utils workspace_exec 9 element  element-desktop";
        # scratchpads
        "${modifier}+t"                 =  "scratchpad show";
        "${modifier}+y"                 =  "move scratchpad";
        # change = "focus ("${modifier}+direction)
        "${modifier}+Left"              = "focus left";
        "${modifier}+Down"              = "focus down";
        "${modifier}+Up"                = "focus up";
        "${modifier}+Right"             = "focus right";
        # move windows ("${modifier}+Shift+direction)
        "${modifier}+Shift+Left"        = "move left";
        "${modifier}+Shift+Down"        = "move down";
        "${modifier}+Shift+Up"          = "move up";
        "${modifier}+Shift+Right"       = "move right";
        # resize windows ("${modifier}+Alt+direction)
        "${modifier}+Mod1+Left"         = "resize shrink width  5 px or 5 ppt";
        "${modifier}+Mod1+Down"         = "resize shrink height 5 px or 5 ppt";
        "${modifier}+Mod1+Up"           = "resize grow   height 5 px or 5 ppt";
        "${modifier}+Mod1+Right"        = "resize grow   width  5 px or 5 ppt";
        # move workspace ("${modifier}+Control+direction)
        "${modifier}+Control+Left"      = "move workspace to output left";
        "${modifier}+Control+Down"      = "move workspace to output down";
        "${modifier}+Control+Up"        = "move workspace to output up";
        "${modifier}+Control+Right"     = "move workspace to output right";
        # switch to workspace ("${modifier}+number)
        "${modifier}+ampersand"         = "workspace 1";
        "${modifier}+eacute"            = "workspace 2";
        "${modifier}+quotedbl"          = "workspace 3";
        "${modifier}+apostrophe"        = "workspace 4";
        "${modifier}+parenleft"         = "workspace 5";
        "${modifier}+minus"             = "workspace 6";
        "${modifier}+egrave"            = "workspace 7";
        "${modifier}+underscore"        = "workspace 8";
        "${modifier}+ccedilla"          = "workspace 9";
        "${modifier}+agrave"            = "workspace 10";
        # = "move container to workspace ("${modifier}+Shift+number)
        "${modifier}+Shift+ampersand"   = "move container to workspace 1";
        "${modifier}+Shift+eacute"      = "move container to workspace 2";
        "${modifier}+Shift+quotedbl"    = "move container to workspace 3";
        "${modifier}+Shift+apostrophe"  = "move container to workspace 4";
        "${modifier}+Shift+parenleft"   = "move container to workspace 5";
        "${modifier}+Shift+minus"       = "move container to workspace 6";
        "${modifier}+Shift+egrave"      = "move container to workspace 7";
        "${modifier}+Shift+underscore"  = "move container to workspace 8";
        "${modifier}+Shift+ccedilla"    = "move container to workspace 9";
        "${modifier}+Shift+agrave"      = "move container to workspace 10";
        # sound volume controls
        "XF86AudioRaiseVolume"          = "exec pactl set-sink-volume @DEFAULT_SINK@ +5%";
        "XF86AudioLowerVolume"	        = "exec pactl set-sink-volume @DEFAULT_SINK@ -5%";
        "XF86AudioMute"		              = "exec pactl set-sink-volume @DEFAULT_SINK@  0%";
        #"XF86AudioMicMute"		          = "";
        "XF86MonBrightnessDown"		      = "exec brightnessctl set 10%-";
        "XF86MonBrightnessUp"		        = "exec brightnessctl set 10%+";
        "XF86Display"		                = "exec arandr";
        #"XF86WLAN"		                  = "";
        #"XF86Tools"		                = "";
        #"XF86Bluetooth"		            = "";
        #"XF86WakeUp"		                = "";
        #"XF86Favorites"		            = "";
      };
    };
  };
}