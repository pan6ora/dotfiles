{ config, ... }:

{
    xdg = {
        enable = true;

        userDirs = {
            enable = true;
            desktop     = "${config.home.homeDirectory}/Temp";
            documents   = "${config.home.homeDirectory}/Temp";
            download    = "${config.home.homeDirectory}/Temp";
            music       = "${config.home.homeDirectory}/Temp";
            pictures    = "${config.home.homeDirectory}/Temp";
            publicShare = "${config.home.homeDirectory}/Temp";
            templates   = "${config.home.homeDirectory}/Temp";
            videos      = "${config.home.homeDirectory}/Temp";
        };
        mimeApps = {
            enable = true;

            defaultApplications = {
                "inode/directory"                   = "Thunar.desktop";
                "application/pdf"                   = "firefox.desktop";
                "image/*"                           = "userapp-feh -ZdF --edit -S name --start-at-OGKHT1.desktop";
                "video/*"                           = "vlc.desktop";
                "x-scheme-handler/around"           = "around.desktop";
                "x-scheme-handler/msteams"          = "teams.desktop";
                # text files
                "text/*"                            = "codium.desktop";
                "application/json"                  = "codium.desktop";
                "application/xml"                   = "codium.desktop";
                "application/x-shellscript"         = "codium.desktop";
                # web links
                "x-scheme-handler/http"             = "firefox.desktop";
                "x-scheme-handler/https"            = "firefox.desktop";
                "x-scheme-handler/chrome"           = "firefox.desktop";
                "application/x-extension-htm"       = "firefox.desktop";
                "application/x-extension-html"      = "firefox.desktop";
                "application/x-extension-shtml"     = "firefox.desktop";
                "application/xhtml+xml"             = "firefox.desktop";
                "application/x-extension-xhtml"     = "firefox.desktop";
                "application/x-extension-xht"       = "firefox.desktop";
            };

        };
    };
}
