{pkgs, config, unstable, ... }:

{
  imports = [
    ./accounts.nix
    ./aliases.nix
    ./colorscheme.nix
    ./i3.nix
    ./xdg.nix
  ];

  xsession.enable = true;

  # add scripts to PATH
  xdg.configFile.scripts.source = ../../scripts;
  home.sessionPath = [
    "${config.home.homeDirectory}/Config/scripts"
    "${config.home.homeDirectory}/Config/scripts/rofi_aliases"
  ];
}
